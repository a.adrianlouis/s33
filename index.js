//1.
fetch('https://jsonplaceholder.typicode.com/todos')
	.then(response => response.json())
	.then(json => console.log(json)) 


//2.
fetch('https://jsonplaceholder.typicode.com/todos')
	.then(response => response.json())
	.then(todos => {
		let todo_list = todos.map(todo => {
			return todo.title
		}) 
	 console.log(todo_list)
	}) 


//3.
fetch('https://jsonplaceholder.typicode.com/todos/1')
	.then(response => response.json())
	.then(todo => console.log(`The item ${todo.title} on the list has a status of ${todo.completed}`))



//4.
fetch('https://jsonplaceholder.typicode.com/todos',
{
	method: "POST",
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		completed: false,
		id: 201,
		title: "Created To Do List Item",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json))
